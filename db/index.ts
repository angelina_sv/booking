import { Client } from 'pg';
import { BookingRange, BookingInfoWithDays } from '../app/types';

async function connection(sql: string, values?: any[]) {
  const client = new Client({
    user: 'postgres',
    host: 'localhost',
    password: '123',
    port: 5432,
    database: 'booking_db',
  });

  await client.connect();
  const result = await client.query(sql, values);
  await client.end();
  return result.rows;
}

async function getRoomsCount() {
  const sql = `
    SELECT COUNT(*)
    FROM rooms;
  `;

  const [{ count }] = await connection(sql);
  return count;
}

function getRoomReservationInfo(room_number: number) {
  const sql = `
  SELECT kk.room_number, kk.freeDays, kkk.bookedDays
  FROM (
    SELECT ttt.room_number, 
      array_agg(
        date_trunc('year', now()::timestamp + interval '1 year')::date + ttt.freeDays - 1
      ) AS freeDays 
    FROM(
      SELECT room_number, generate_series(1, 365, 1) AS freeDays
      FROM rooms
      EXCEPT
      SELECT tt.rn, unnest(tt.bookedDays) 
      FROM(
        SELECT tmp.rn, array_agg(tmp.n) AS bookedDays
        FROM(
          SELECT room_number AS rn, generate_series(start_day, end_day, 1) AS n 
          FROM booking
        ) tmp 
        GROUP BY tmp.rn
      ) tt
      ORDER BY room_number, freeDays
    ) ttt
    GROUP BY ttt.room_number
  ) kk JOIN (
    SELECT b.room_number, 
    CASE WHEN b.bookedDays = '{NULL}' THEN '{}' ELSE b.bookedDays END
    FROM (
      SELECT rooms.room_number, array_agg(tmp.n) AS bookedDays
      FROM rooms LEFT JOIN (
        SELECT room_number AS rn, 
          generate_series(
            date_trunc('year', now()::timestamp + interval '1 year')::date + start_day - 1, 
            date_trunc('year', now()::timestamp + interval '1 year')::date + end_day - 1, interval '1 day'
          )::date AS n 
        FROM booking
      ) tmp ON tmp.rn = rooms.room_number
      GROUP BY rooms.room_number
    ) b
    ORDER BY b.room_number
  ) kkk 
  ON kk.room_number = kkk.room_number
  WHERE kk.room_number = $1;
`;

  const values = [room_number];
  return connection(sql, values);
}

function getRoomsBookingInfo(limit: number, offset: number) {
  const sql = `
    SELECT kk.room_number, kk.freeDays, kkk.bookedDays
    FROM (
      SELECT ttt.room_number, 
        array_agg(
          date_trunc('year', now()::timestamp + interval '1 year')::date + ttt.freeDays - 1
        ) AS freeDays 
      FROM(
        SELECT room_number, generate_series(1, 365, 1) AS freeDays
        FROM rooms
        EXCEPT
        SELECT tt.rn, unnest(tt.bookedDays) 
        FROM(
          SELECT tmp.rn, array_agg(tmp.n) AS bookedDays
          FROM(
            SELECT room_number AS rn, generate_series(start_day, end_day, 1) AS n 
            FROM booking
          ) tmp 
          GROUP BY tmp.rn
        ) tt
        ORDER BY room_number, freeDays
      ) ttt
      GROUP BY ttt.room_number
    ) kk JOIN (
      SELECT b.room_number, 
      CASE WHEN b.bookedDays = '{NULL}' THEN '{}' ELSE b.bookedDays END
      FROM (
        SELECT rooms.room_number, array_agg(tmp.n) AS bookedDays
        FROM rooms LEFT JOIN (
          SELECT room_number AS rn, 
            generate_series(
              date_trunc('year', now()::timestamp + interval '1 year')::date + start_day - 1, 
              date_trunc('year', now()::timestamp + interval '1 year')::date + end_day - 1, interval '1 day'
            )::date AS n 
          FROM booking
        ) tmp ON tmp.rn = rooms.room_number
        GROUP BY rooms.room_number
      ) b
      ORDER BY b.room_number
    ) kkk 
    ON kk.room_number = kkk.room_number
    LIMIT $1 OFFSET $2;
  `;
  const values = [limit, offset];
  return connection(sql, values);
}

function getReservationInfo(id: number) {
  const sql = `
    SELECT id, room_number,
      date_trunc('year', now()::timestamp + interval '1 year')::date + start_day - 1 AS start_day,
      date_trunc('year', now()::timestamp + interval '1 year')::date + end_day - 1 AS end_day
    FROM booking
    WHERE id = $1;
  `;

  const values = [id];
  return connection(sql, values);
}

async function checkReservation(id: number) {
  const sql = `
    SELECT *
    FROM booking
    WHERE id = $1;
  `;

  const values = [id];
  const result = await connection(sql, values);
  return result.length;
}

function checkReservationForDay(day: number) {
  const sql = `
  SELECT room_number FROM rooms
  EXCEPT
  SELECT room_number FROM booking
  WHERE $1 BETWEEN start_day AND end_day;
  `;

  const values = [day];
  return connection(sql, values);
}

async function getFreeRooms(days: BookingRange<number>) {
  let values;
  let sql;
  if (days.id) {
    sql = `
      SELECT room_number
      FROM rooms
      EXCEPT
      SELECT room_number
      FROM booking
      WHERE (start_day BETWEEN $1 AND $2) AND 
            (end_day BETWEEN $1 AND $2) AND
            booking.id <> $3;
  `;

    values = [days.start_day, days.end_day, days.id];
  } else {
    sql = `
      SELECT room_number
      FROM rooms
      EXCEPT
      SELECT room_number
      FROM booking
      WHERE (start_day BETWEEN $1 AND $2) AND 
            (end_day BETWEEN $1 AND $2);
  `;

    values = [days.start_day, days.end_day];
  }

  return connection(sql, values);
}

async function createReservation(bookingInfo: BookingInfoWithDays) {
  const sql = `
    INSERT INTO booking (room_number, start_day, end_day)
    VALUES ($1, $2, $3)
    RETURNING *;
  `;

  const values = [bookingInfo.room_number, bookingInfo.start_day, bookingInfo.end_day];
  return connection(sql, values);
}

function changeReservation(bookingInfo: BookingInfoWithDays) {
  const sql = `
    UPDATE booking
    SET room_number = $1,
      start_day = $2,
      end_day = $3
    WHERE id = $4;
  `;

  const values = [bookingInfo.room_number, bookingInfo.start_day, bookingInfo.end_day, bookingInfo.id];
  return connection(sql, values);
}

function deleteReservation(id: number) {
  const sql = `
    DELETE FROM booking
    WHERE id = $1;
  `;

  const values = [id];
  return connection(sql, values);
}

export {
  getRoomsCount,
  getRoomReservationInfo,
  getRoomsBookingInfo,
  getReservationInfo,
  checkReservation,
  checkReservationForDay,
  getFreeRooms,
  createReservation,
  changeReservation,
  deleteReservation,
};
