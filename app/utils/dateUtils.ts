const DAYS_IN_YEAR = 365;

function createArr(start: number = 1, end: number = DAYS_IN_YEAR, step: number = 1): number[] {
  const result: number[] = [];

  for (let i = start; i <= end; i += step) {
    result.push(i);
  }

  return result;
}

function formatDate(date: Date): string {
  const month = date.getMonth() < 9 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
  const day = date.getDate() < 9 ? `0${date.getDate()}` : date.getDate();
  return `${date.getFullYear()}-${month}-${day}`;
}

function dateFromDay(day: number): string {
  const startDate: Date = new Date(2021, 0);
  const result: any = new Date(startDate.setDate(day));

  return formatDate(result);
}

function dayFromDate(date: Date): number {
  const newYear = new Date(2021, 0, 1);

  return (Math.floor((date.getTime() - newYear.getTime()) / (1000 * 60 * 60 * 24)) + 1);
}

function createDate(str: any): Date {
  return new Date(str);
}

export {
  createArr,
  dateFromDay,
  dayFromDate,
  formatDate,
  createDate,
};
