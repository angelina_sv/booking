/* eslint-disable @typescript-eslint/naming-convention */
import * as express from 'express';
import {
  getRoomInfo,
  deleteReservation,
  getFreeAndBookedDates,
  createReservation,
  changeReservation,
  paginate,
} from './service';
import { createDate } from './utils/dateUtils';

const router = express.Router();

router.get('/', async (req, res) => {
  const { currentPage, numberOfElements } = req.body;
  const result = await paginate(currentPage, numberOfElements);

  res.send(result);
});

router.get('/calendar', async (_req, res) => {
  const result = await getFreeAndBookedDates();

  res.send(result);
});

router.post('/room', async (req, res) => {
  const start_day = await createDate(req.body.start_day);
  const end_day = await createDate(req.body.end_day);
  const result = await createReservation({ start_day, end_day });

  res.send(result);
});

router.delete('/booking/:id', async (req, res) => {
  const { id } = req.params;
  await deleteReservation(Number(id));

  res.send();
});

router.patch('/room', async (req, res) => {
  const { id, room_number } = req.body;
  const start_day = await createDate(req.body.start_day);
  const end_day = await createDate(req.body.end_day);
  const result = await changeReservation({
    id,
    room_number,
    start_day,
    end_day,
  });

  res.send(result);
});

router.get('/room/:id', async (req, res) => {
  const id = parseInt(req.params.id, 10);
  const result = await getRoomInfo(id);

  res.send(result);
});

export { router };
