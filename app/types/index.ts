interface BookingRange<TDayFormat> {
  id?: number;
  start_day: TDayFormat;
  end_day: TDayFormat;
}

interface Room {
  id: number;
  room_number: number;
}

interface FreeAndBookedDates<T> {
  room_number?: number;
  freeDates?: T[];
  bookedDates: T[];
}

interface Paginate {
  totalPages: number;
  elements: FreeAndBookedDates<string>[];
}

type BookingInfo = Room & BookingRange<Date>;
type BookingInfoWithDays = Room & BookingRange<number>;

export {
  BookingRange,
  Room,
  FreeAndBookedDates,
  Paginate,
  BookingInfo,
  BookingInfoWithDays,
};
