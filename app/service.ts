/* eslint-disable no-param-reassign */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-restricted-syntax */
import * as db from '../db';
import {
  BookingRange,
  Room,
  FreeAndBookedDates,
  Paginate,
  BookingInfo,
} from './types';
import {
  createArr,
  dateFromDay,
  dayFromDate,
} from './utils/dateUtils';

const daysOfTheYear: number[] = createArr();

async function getFreeAndBookedDates(): Promise<FreeAndBookedDates<string>> {
  const freeDates: string[] = [];
  const bookedDates: string[] = [];

  for (const day of daysOfTheYear) {
    // eslint-disable-next-line no-await-in-loop
    const checkDay = await db.checkReservationForDay(day);

    if (checkDay.length === 0) {
      bookedDates.push(dateFromDay(day));
    } else {
      freeDates.push(dateFromDay(day));
    }
  }

  return {
    freeDates,
    bookedDates,
  };
}

async function getRoomsBookingInfo(limit: number = 50, offset: number = 0): Promise<FreeAndBookedDates<string>[]> {
  const roomsBookingInfo = await db.getRoomsBookingInfo(limit, offset);

  return roomsBookingInfo;
}

async function getRoomInfo(roomNumber: number): Promise<FreeAndBookedDates<string>[]> {
  const roomInfo = await db.getRoomReservationInfo(roomNumber);

  return roomInfo;
}

async function paginate(
  currentPage: number = 1,
  numberOfElements: number = 10,
): Promise<Paginate> {
  const totalNumberOfElements = await db.getRoomsCount();

  if (numberOfElements < 1) {
    numberOfElements = 1;
  } else if (numberOfElements > totalNumberOfElements) {
    numberOfElements = totalNumberOfElements;
  }

  const maxPage = Math.floor(totalNumberOfElements / numberOfElements) + 1;

  if (currentPage > maxPage) {
    currentPage = maxPage;
  } else if (currentPage < 1) {
    currentPage = 1;
  }

  const offset = numberOfElements * (currentPage - 1);
  const roomsBookingInfo = await getRoomsBookingInfo(numberOfElements, offset);

  return {
    totalPages: maxPage - 1,
    elements: roomsBookingInfo,
  };
}

// eslint-disable-next-line consistent-return
async function createReservation(dates: BookingRange<Date>): Promise<false | BookingInfo> {
  const start_day: number = dayFromDate(dates.start_day);
  const end_day: number = dayFromDate(dates.end_day);
  const checkAvailabilityDates = await db.getFreeRooms({ start_day, end_day });

  if (checkAvailabilityDates.length === 0) {
    return false;
  }

  const obj: Room = checkAvailabilityDates[0];
  const { room_number } = obj;
  const reservation = await db.createReservation({
    id: 0,
    room_number,
    start_day,
    end_day,
  });

  const reservationInfo = reservation[0];
  const { id } = reservationInfo;

  return {
    id,
    room_number,
    start_day: dates.start_day,
    end_day: dates.end_day,
  };
}

async function changeReservation(bookingInfo: BookingInfo): Promise<false | BookingInfo> {
  const start_day: number = dayFromDate(bookingInfo.start_day);
  const end_day: number = dayFromDate(bookingInfo.end_day);
  const { id } = bookingInfo;
  const checkReservation = await db.checkReservation(bookingInfo.id);

  if (checkReservation === 0) {
    return false;
  }

  const checkAvailabilityDates = await db.getFreeRooms({ start_day, end_day });
  const obj: Room = checkAvailabilityDates[0];
  const { room_number } = obj;

  await db.changeReservation({
    id,
    room_number,
    start_day,
    end_day,
  });

  const newReservationInfo = await db.getReservationInfo(id);

  return newReservationInfo[0];
}

async function deleteReservation(id: number): Promise<void> {
  await db.deleteReservation(id);
}

export {
  getRoomInfo,
  deleteReservation,
  getFreeAndBookedDates,
  createReservation,
  changeReservation,
  getRoomsBookingInfo,
  paginate,
};
