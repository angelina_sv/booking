CREATE TABLE public.rooms
(
    id serial NOT NULL,
    room_number integer NOT NULL,
    CONSTRAINT rooms_pkey PRIMARY KEY (id),
    CONSTRAINT room_number_constraint UNIQUE (room_number)
);

ALTER TABLE public.rooms
    OWNER to postgres;


CREATE TABLE public.booking
(
    id serial NOT NULL,
    room_number integer NOT NULL,
    start_day smallint NOT NULL,
    end_day smallint NOT NULL,
    CONSTRAINT booking_pkey PRIMARY KEY (id),
    CONSTRAINT room_number_fk FOREIGN KEY (room_number)
        REFERENCES public.rooms (room_number) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

ALTER TABLE public.booking
    OWNER to postgres;


INSERT INTO rooms (room_number) SELECT * FROM generate_series(100,150);